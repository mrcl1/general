# Bug report
## Checklist
<!--Put an 'X' between the brackets to mark it -->
- [ ] I checked that this bug wasn't reported yet

## Reporter
<!--If you're reporting this bug in name of someone else, fill this out. If not, delete this section-->
#### Discord name:
#### Report date:

## Description
<!--Describe the bug-->

## Reproduction steps
<!--Write down the steps to reproduce this bug-->
1. Step one
2. Step two

## Media
<!-- Put screenshots or videos here-->
